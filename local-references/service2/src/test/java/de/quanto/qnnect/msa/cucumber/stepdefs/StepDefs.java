package de.quanto.qnnect.msa.cucumber.stepdefs;

import de.quanto.qnnect.msa.Service2App;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = Service2App.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
