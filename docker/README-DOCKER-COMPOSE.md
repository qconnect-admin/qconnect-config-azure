# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:
- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:
- QConnectCore (microservice application)
- QConnectCore's no database
- QConnectGateway (gateway application)
- QConnectGateway's mongodb database

### Additional Services:

- [JHipster Console](http://localhost:5601)
- [Zipkin](http://localhost:9411)
